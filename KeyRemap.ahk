#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#Persistent
#SingleInstance Force

; Desktop scolling(Ctrl + Win + Left/Right) ==> PageUp(prev)/PageDown(next)
PgUp::SendInput, ^#{Left}
PgDn::SendInput, ^#{Right}

; Close current window/application ==> RAlt + Back quote
!`::SendInput, !{F4}

; Menu key(Shift + F10) ==> RAlt
; Some laptop keyboard layouts don't have menu keys, this is my answer
;RAlt::SendInput, +{F10}

; Capture the 3 finger click which is sent to Windows as Left windows + Shift + Ctrl + F22
; Credit: vainglorious11[Reddit]
; Link: https://www.reddit.com/r/Dell/comments/3ri4pm/enable_3finger_tap_as_middle_click_on_xps_15_and/
;#^+F22::
;	Click Middle
;Return

; Ctrl + Space, to set focused window to always on top
;^SPACE::  Winset, Alwaysontop, , A

; Disables both left and right window keys
;RWin::
;LWin::
